@extends('layouts.master')

@section('title')
Halaman Tambah Cast   
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="age">Age</label>
      <input type="number" class="form-control @error('age') is-invalid @enderror" id="age" name="age">
    </div>
    @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" id="bio" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection