@extends('layouts.master')

@section('title')
Buat Account Baru   
@endsection
    {{-- <h1> Buat Account Baru </h1> --}}

@section('content')
    <h3> Sign Up Form </h3>
    
    <form action="/welcome" method="post">
        @csrf
        <label for="firstName"> First name: </label> <br>
        <input type="text" name="firstName" id="firstName"> <br> <br>

        <label for="lastName"> Last name: </label> <br>
        <input type="text" name="lastName" id="lastName"> <br> <br>

        <label> Gender: </label> <br>
        <input type="radio" value="male" name="gender" id="male"><label for="male"> Male </label><br>
        <input type="radio" value="female" name="gender" id="female"><label for="female"> Female </label><br>
        <input type="radio" value="other" name="gender" id="other"><label for="other"> Other </label><br> <br>

        <label> Nationality: </label> 
        <select name="nationality">
            <option value="indonesia"> Indonesia </option>
            <option value="jepang"> Jepang </option>
            <option value="malaysia"> Malaysia </option>
            <option value="singapura"> Singapura </option>
        </select> <br> <br>

        <label> Language Spoken: </label> <br>
        <input type="checkbox" value="bhsIndonesia" name="language" id="bhsIndonesia"><label for="bhsIndonesia"> Bahasa Indonesia </label><br>
        <input type="checkbox" value="english" name="language" id="english"><label for="english"> English </label><br>
        <input type="checkbox" value="other" name="language" id="other"><label for="other"> Other </label><br> <br>

        <label for="bio"> Bio: </label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>

        <button type="submit"> Sign Up </button>
    </form>
@endsection