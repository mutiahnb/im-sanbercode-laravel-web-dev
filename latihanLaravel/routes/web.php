<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//entpoint/route
Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function() {
    return view('page.table');
});

Route::get('/data-table', function() {
    return view('page.data-table');
});

// CRUD
// C => Create Data
// Route mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);

// Insert data inputan ke database table cast
Route::post('/cast', [CastController::class, 'store']);

// R => Read Data
// Route untuk menampilkan semua pada table cast di DB
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// U => Update Data
// Route yang mengarah ke form edit data dengan params id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// D = Delete Data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

//testing master template
// Route::get('/master', function(){
//     return view('layouts.master');
// });