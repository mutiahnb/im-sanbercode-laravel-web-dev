<?php
require_once ("animal.php");
require_once ("frog.php");
require_once ("ape.php");

$sheep = new animal ("Shaun");

echo "Name : " . $sheep->name . "<br>"; // "Shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded ."<br> <br>"; // "No"

$kodok = new frog ("Buduk");

echo "Name : " . $kodok->name . "<br>"; // "Buduk"
echo "Legs : " . $kodok->legs . "<br>"; // 4
echo "Cold Blooded : " . $kodok->cold_blooded ."<br>"; // "No"
echo "Jump : " . $kodok->jump() . "<br> <br>"; // "Hop Hop"

$sungokong = new ape ("Kera Sakti");

echo "Name : " . $sungokong->name . "<br>"; // "Kera Sakti"
echo "Legs : " . $sungokong->legs . "<br>"; // 2
echo "Cold Blooded : " . $sungokong->cold_blooded ."<br>"; // "No"
echo "Yell : " . $sungokong->yell() . "<br"; // "Auooo"